package org.ananaseg.chklistfromsms.checklistitems;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class CheckListContent {

    /**
     * An array of sample (CheckList) items.
     */
    public static List<CheckListItem> ITEMS = new ArrayList<CheckListItem>();

    /**
     * A map of sample (CheckList) items, by ID.
     */
  //  public static Map<String, CheckListItem> ITEM_MAP = new HashMap<String, CheckListItem>();

    static {
        for(int i = 0; i < 10; i++) {
            addItem(new CheckListItem(String.valueOf(i), "Item "+i, false));
        }
    }

    private static void addItem(CheckListItem item) {
        ITEMS.add(item);
      //  ITEM_MAP.put(item.id, item);
    }

    /**
     * A CheckList item representing a piece of content.
     */
    public static class CheckListItem {
        public String id;
        public String content;
        public boolean checked;

        public CheckListItem(String id, String content, boolean checked) {
            this.id = id;
            this.content = content;
            this.checked = checked;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
